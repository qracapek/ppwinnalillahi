from django.test import TestCase, Client
from django.urls import resolve
from .views import story7


# Create your tests here.

class Story7UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('')
        self.page_content = self.response.content.decode('utf8')

    def test_story7_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story7_check_template_used(self):
        self.assertTemplateUsed(self.response, 'story7.html')
    
    def test_story7_check_function_used(self):
        found = resolve('/')
        self.assertEqual(found.func, story7)
    
    def test_story7_content_exists(self):
        self.assertIn('+ Activities', self.page_content)
        self.assertIn('+ Experiences', self.page_content)
        self.assertIn('+ Achievements', self.page_content)
        self.assertIn('+ Struggles', self.page_content)
        

from django.urls import path
from .views import logIn, signUp, logOut, homepage

urlpatterns = [
    path('', logIn, name='login'),
    path('signup/', signUp, name='signup'),
    path('logout/', logOut, name='logout'),
    path('homepage/', homepage, name='homepage'),
]

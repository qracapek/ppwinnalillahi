from django.urls import path
from .views import story8, fungsi_data

app_name = 'story8'

urlpatterns = [
    path('', story8, name='story8'),
    path('data/', fungsi_data, name='fungsi_data')
]
